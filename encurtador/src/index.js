import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);

const express = require('express');

  const PORT = 3000;
  const HOST = '0.0.0.0';
  
  
  const app = express();

  app.get('/', (req, res) => {
    res.send('Hello World');
  });

  app.listen(PORT, HOST);