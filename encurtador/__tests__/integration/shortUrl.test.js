const { shortUrl } = require("../../models/");

describe('urlshortener', () => {
    it("would like to shorten an extensive url", async () => {
        const full = await shortUrl.full.create({
            type: String,
            required: true
        })

        const short = await shortUrl.short.create({
            type: String,
            required: true,
            default: shortId.generate
        })

        expect(short).toBe(short);
    });
});