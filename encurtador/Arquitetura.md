#### Documento de Arquitetura de Software

**Objetivo**

Este documento fornece uma visão arquitetural do sistema, utilizando a arquitetura mais adequada em relação aos diferentes aspectos do sistema. Este, por sua vez, pretende capturar e transmitir as decisões arquiteturas significativas que foram tomadas em relação ao sistema que atuará como um encurtador de Url.

O encurtador de Url atua como uma ferramenta que permite compactar links muitos extensos para assim compartilhá-los sem limitações, facilitando dessa forma o fluxo de acesso a informação compartilhada.

O objetivo do teste prático é desenvolver uma ferramenta que venha suprir essa necessidade. Para tal, optou-se pela arquitetura MVC - Model, Controller e View, devido a sua otimização na velocidade entre as requisições feitas pelo usuário. Nesse sentido, o sistema será dividido em três camadas, sendo elas definidas abaixo:

> A **Model** gerenciará e controlará a forma como os dados se comportam no sistema. Na ferramenta desenvolvida ela receberá as Urls, fará a validação para devolver a informação mais adequada ao usuário.

> A **controller** é responsável por intermediar as requisições enviadas pela view com as respostas fornecidas pela Model. No sistema apresentado, ela processará os dados (as Urls inseridas) e repassará as informações para as demais camadas.

> A **view** é responsável por apresentar as informações de forma visual ao usuário. No sistema, ela servirá como a interface para que o usuário tenha direcionamento quanto a navegação, onde inserir as Urls e onde visualizar as respostas.  

